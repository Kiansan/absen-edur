<!DOCTYPE html>
<html lang="en">
<head>
<!-- meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
 
<!-- costum css -->
<link rel="stylesheet" href="css/style.css">
</head>
  
<body>
    <section class="container-fluid">
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
            <form class="form-container"  action="/user" method="POST">
            @csrf
                <h4 class="text-center font-weight-bold"> Form Absensi </h4>
                <div class="form-group">
                    <label for="name">Nama</label></br>
                    <input type="text" class="form-control" id="name" placeholder="Masukkan Nama">
                    @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                     @enderror
                </div>
            </form>
        </section>
        </section>
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
        <form class="form-container"  action="/user" method="POST">
        <div class="form-group">
                    <label for="InputEmail">Alamat Email</label></br>
                    <input type="email" class="form-control" id="InputEmail" aria-describeby="emailHelp" placeholder="Masukkan email">
                    @error('email')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                    @enderror
                </div>
        </form>    
        </section>  
        </section>      
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
        <div class="form-group">
        <form class="form-container">
        <label> Instansi </label></br>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                <label class="form-check-label" for="exampleRadios1">
                SMKN 2 MAGELANG
            </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label" for="exampleRadios2">
                UNIVERSITAS MUHAMMADIYAH YOGYAKARTA
            </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label" for="exampleRadios2">
                SMKN JATENG
            </label>
            </div>
        </form>
        </section>  
        </section>   
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
        <form class="form-container">
        <label> Kelas </label></br>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                <label class="form-check-label" for="exampleRadios1">
                RPL
            </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label" for="exampleRadios2">
                MASAK MASAKAN
            </label>
            </div>
        </form>    
        </section>  
        </section>      
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
        <form class="form-container">
        <label> Presensi </label></br>
        <div class="form-check">
                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                <label class="form-check-label" for="exampleRadios1">
                Hadir
            </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
            <label class="form-check-label" for="exampleRadios2">
                Tidak Hadir
            </label>
            </div>
        </section>  
        </section> 
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-4">
            <a href="/formm"> <button type="submit" class="btn btn-primary btn-block">Berikutnya</button><a>
        </section>
        </section>
    </section>        
 
    <!-- Bootstrap requirement jQuery pada posisi pertama, kemudian Popper.js, danyang terakhit Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>