<!DOCTYPE html>
<html lang="en">
<head>
<!-- meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
 
<!-- costum css -->
<link rel="stylesheet" href="css/style.css">
</head>
  
<body>
    <section class="container-fluid">
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
            <form class="form-container">
                <h4 class="text-center font-weight-bold"> Form Absensi </h4>
                <div class="form-group">
                    <label for="name">Target Hari Ini</label></br>
                    <input type="text" class="form-control" id="name" placeholder="Target Hari Ini">
                </div>
            </form>
        </section>
        </section>
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
        <form class="form-container">
        <div class="form-group">
                    <label for="InputEmail">Aktivitas Hari Ini</label></br>
                    <input type="text" class="form-control" id="name" aria-describeby="emailHelp" placeholder="Aktivitas Hari Ini">
                </div>
        </form>    
        </section>  
        </section>
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-8">
        <form class="form-container">
        <div class="mb-3">
            <label for="formFile" class="form-label">Foto Kegiatan/Aktivitas</label></br></br>
            <input type="file" id="myfile" name="myfile" multiple>
        </div>
        </form>    
        </section>  
        </section>  
        <section class="row justify-content-center">
        <section class="col-12 col-sm-6 col-md-4">
            <a href="/form"> <button type="submit" class="btn btn-primary btn-block">Kembali</button><a>
        </section>
        <section class="col-12 col-sm-6 col-md-4">
            <a href="#"> <button type="submit" class="btn btn-primary btn-block">Kirim</button><a>
        </section>
        </section>       
    </section>        
 
    <!-- Bootstrap requirement jQuery pada posisi pertama, kemudian Popper.js, danyang terakhit Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>