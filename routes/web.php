<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('welcome');
});

Route::get('/form', function() {
    return view('form');
});

Route::get('/formm', function() {
    return view('formm');
});

Route::get('/index', function() {
    return view('layout.page.index');
});

Route::get('/admin', function(){
    return view('layout.admin');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::get('user/home', 'HomeController@userHome')->name('user.home')->middleware('is_user');

Route::get('/user', 'userController@index');
Route::get('/user/create', 'userController@create');
Route::post('/user', 'userController@store');
Route::get('/user/{user_id}', 'userController@show');
Route::get('/user/{user_id}/edit', 'userController@edit');
Route::put('user/{user_id}', 'userController@update');
Route::delete('user/{user_id}', 'userController@destroy');

Route::resource('/learn', 'LearnController');
